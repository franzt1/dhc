#include <iostream>
#include <string>
#include "filters.h" // StringSink
#include "hex.h" // HexEncoder
#include "aes.h" // AES
#include "modes.h" // ECB_Mode
#include "sha.h"
#include "base64.h"
#include "files.h" // FileSink
#include "osrng.h" // BlockingRNG
#include "integer.h" // Integer
#include "nbtheory.h" // RabinMillerTest
#include "BlumBlumShubGenerator.h"
#include "modarith.h"


using namespace std;
using namespace CryptoPP;

void DataFlowExercise() {
	cout << endl
	     << "Data Flow Exercise:" << endl
	     << "===================" << endl << endl;

}

void HashExercise() {
}

void EncryptionExercise() {
}


Integer modexp(const Integer& a, const Integer& b, const Integer& n) {

}

void IntegerExercise() {
	cout << endl
		 << "IntegerExercise:" << endl
		 << "================" << endl << endl;

}

void RNGExercise() {
	cout << "RNGExercise:" << endl
	     << "============" << endl << endl;

}
int dh(int){
  Integer randA, randB, p,G,M1,M2;

  Integer m("4294967295"), n("0x1000000000000000000000000000000"), j;
  j = 1999;

  ModularArithmetic ma(j);
}
int main(){
  //cout << "n+m mod j: " << ma.Add(n, m) << endl;
  //cout << "n-m mod j: " << ma.Subtract(n, m) << endl;
  //cout << "n*m mod j: " << ma.Multiply(n, m) << endl;
  //cout << "n/m mod j: " << ma.Divide(n, m) << endl;
  //cout << "n%m mod j: " << ma.Reduce(n, m) << endl;
  //cout << "n^m mod j: " << ma.Exponentiate(n, m) << endl;

	//DataFlowExercise();
	//EncryptionExercise();
	//HashExercise();
	//IntegerExercise();
	//RNGExercise();
	return 0;
}
