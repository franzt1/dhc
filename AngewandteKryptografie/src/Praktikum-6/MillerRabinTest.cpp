/*
 * MillerRabinTest.cpp
 */
#include <iostream>
#include <cassert>
#include "MillerRabinTest.h"

using namespace std;

Integer MillerRabinTest::boundedExponentation(const Integer& b, const Integer& e, const Integer& bound) const {
}

bool MillerRabinTest::isPerfectPower(const Integer& n, Integer& b, Integer& e) const {
}

Integer MillerRabinTest::modularExponentation
(
 const Integer& a,
 const Integer& b,
 const Integer& n
) const
{
}

bool MillerRabinTest::witness(const Integer& a, const Integer& n) const {
}

bool MillerRabinTest::isPrime(PRNG* rng, const Integer& n, unsigned int s) const {
}


Integer MillerRabinTest::searchBase(const Integer& n, const Integer& e) const {
}
