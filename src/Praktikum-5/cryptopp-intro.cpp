#include <iostream>
#include <string>
#include "filters.h" // StringSink
#include "hex.h" // HexEncoder
#include "aes.h" // AES
#include "modes.h" // ECB_Mode
#include "sha.h"
#include "base64.h"
#include "files.h" // FileSink
#include "osrng.h" // BlockingRNG
#include "integer.h" // Integer
#include "nbtheory.h" // RabinMillerTest
#include "BlumBlumShubGenerator.h"

using namespace std;
using namespace CryptoPP;

void DataFlowExercise() {
	cout << endl
	     << "Data Flow Exercise:" << endl
	     << "===================" << endl << endl;

}

void HashExercise() {
}

void EncryptionExercise() {
}


Integer modexp(const Integer& a, const Integer& b, const Integer& n) {

}

void IntegerExercise() {
	cout << endl
		 << "IntegerExercise:" << endl
		 << "================" << endl << endl;

}

void RNGExercise() {
	cout << "RNGExercise:" << endl
	     << "============" << endl << endl;

}

int main() {
	//DataFlowExercise();
	//EncryptionExercise();
	//HashExercise();
	//IntegerExercise();
	//RNGExercise();
	return 0;
}
